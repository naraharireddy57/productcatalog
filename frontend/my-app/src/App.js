import React from 'react';
import './App.css';

import { Switch } from 'react-router-dom';
import { BrowserRouter } from 'react-router-dom';
import { Route } from 'react-router-dom';
import Landingpage from './crud_components/landingpage/landingpage.js';
import Postdata from './crud_components/post/post.js';
import Getdata from './crud_components/get/get.js';
import Putdata from './crud_components/put/put.js';
import Deletedata from './crud_components/delete/delete.js';
import ProductList from './crud_components/get/productdetails.js';
import GetAlldata from './crud_components/get/getAll.js';
import Navigate from './crud_components/navbar/navbar.js';
import StatusCode from './crud_components/successpage/successpage.js';
import deleteStatusCode from './crud_components/successpage/deletestatuscode';
import putStatusCode from './crud_components/successpage/putstatuscode';


function App() {
  return (
    <BrowserRouter>
    <Navigate/>
   
      <Switch>
        <Route path='/' exact component={Landingpage} />
        <Route path='/postdetails' exact component={Postdata} />
        <Route path='/statuscode' exact component={StatusCode}/>
        <Route path='/getdetails' exact component={Getdata} />
        <Route path='/productdetails' exact component={ProductList} />
        <Route path='/getalldetails' exact component={GetAlldata} />
        <Route path='/putdetails' exact component={Putdata} />
        <Route path='/putStatusCode' exact component={putStatusCode}/>
        <Route path='/deletedetails' exact component={Deletedata} />
        <Route path='/deleteStatusCode' exact component={deleteStatusCode}/>
        
        
      </Switch>
    </BrowserRouter>

  );
}

export default App;
