
import React from "react";
import { Link } from "react-router-dom";
import "./post.css";

import {
    _product_name_checker,
    _product_description_checker,
    _product_price_checker,
    _discount_checker,
    _merchant_name_checker,
    _quantity_checker,
    _return_object_keys,
} from "../validations/helperFunction.js";


class Postdata extends React.Component {
    state = {
        product_name: "",
        product_name_err: "",
        product_description: "",
        product_description_err: "",
        product_price: "",
        product_price_err: "",
        discount: "",
        discount_err: "",
        merchant_name: "",
        merchant_name_err: "",
        quantity: "", 
        quantity_err: "",
    };

    registerSuccess = () => {
        const { history } = this.props
        history.push("/statuscode")
    }

    apiCallFail = (data) => {
        this.setState({ isSignUp: true, error_msg: data.status_message })
    }

    registerApiCall = async (event) => {
        event.preventDefault();
        console.log(this.state);


        const { product_name, merchant_name, product_price, product_description, discount, quantity } = this.state
        const url = "http://localhost:5000/postdetails"
        const productdetails = {
            product_name,
            product_description,
            product_price:parseInt(product_price),
            discount:parseInt(discount),
            merchant_name,
            quantity:parseInt(quantity),
        }
        const option = {
            method: "POST",
            body: JSON.stringify(productdetails),
            headers: {
                "Content-Type": "application/json",
                Accept: "application/json"
            }
        }
        const response = await fetch(url, option)
        const data = await response.json()
        console.log(data);
        if (data.Statuscode === 200) {
            this.registerSuccess()
        }
        else {
            this.apiCallFail(data)
        }
    }

    onChangeproduct_name = (event) => {
        this.setState({ product_name: event.target.value, product_name_err: ""})
    }
    validateproduct_name = () => {
        const product_name = this.state.product_name;
        const product_name_errors = _product_name_checker(product_name);
        const is_product_name_validated =
        _return_object_keys(product_name_errors).length === 0;
        console.log(product_name_errors);
        console.log(is_product_name_validated);
        if (!is_product_name_validated) {
          // product_name validation failed
          this.setState({ product_name_err: product_name_errors.product_name });
        }
      };

    onChangeproduct_description = (event) => {
        this.setState({ product_description: event.target.value, product_description_err: "" })
    }
    validateproduct_description = () => {
        const product_description = this.state.product_description;
        const product_description_errors = _product_description_checker(product_description);
        const is_product_description_validated =
        _return_object_keys(product_description_errors).length === 0;
        console.log(product_description_errors);
        console.log(is_product_description_validated);
        if (!is_product_description_validated) {
          // firstname validation failed
          this.setState({ product_description_err: product_description_errors.product_description });
        }
      };
    

    onChangeproduct_price = (event) => {
        this.setState({ product_price: event.target.value,product_price_err: "" })
    }
    validateproduct_price = () => {
        const product_price = this.state.product_price;
        const product_price_errors = _product_price_checker(product_price);
        const is_product_price_validated = _return_object_keys(product_price_errors).length === 0;
        console.log(product_price_errors);
        console.log(is_product_price_validated);
        if (!is_product_price_validated) {
          // emp_id validation failed
          this.setState({ product_price_err: product_price_errors.product_price });
        }
      };

    onChangediscount = (event) => {
        this.setState({ discount: event.target.value, discount_err: "" })
    }
    validatediscount = () => {
        const discount = this.state.discount;
        const discount_errors = _discount_checker(discount);
        const is_discount_validated = _return_object_keys(discount_errors).length === 0;
        console.log(discount_errors);
        console.log(is_discount_validated);
        if (!is_discount_validated) {
          // emp_id validation failed
          this.setState({ discount_err: discount_errors.discount});
        }
      };

    onChangemerchant_name = (event) => {
        this.setState({ merchant_name: event.target.value, merchant_name_err: "" })
    }
    validatemerchant_name = () => {
        const merchant_name = this.state.merchant_name;
        const merchant_name_errors = _merchant_name_checker(merchant_name);
        const is_merchant_name_validated =
        _return_object_keys(merchant_name_errors).length === 0;
        console.log(merchant_name_errors);
        console.log(is_merchant_name_validated);
        if (!is_merchant_name_validated) {
         
          this.setState({ merchant_name_err: merchant_name_errors.merchant_name});
        }
      };

    onChangequantity = (event) => {
        this.setState({ quantity: event.target.value, quantity_err: "" })
    }
    validatequantity = () => {
        const quantity = this.state.quantity;
        const quantity_errors = _quantity_checker(quantity);
        const is_quantity_validated =
        _return_object_keys(quantity_errors).length === 0;
        console.log(quantity_errors);
        console.log(is_quantity_validated);
        if (!is_quantity_validated) {
          
          this.setState({ quantity_err: quantity_errors.quantity });
        }
      };

    render() {
         
        return (
            
            <div className="response">
                <div className="login-page">
                    <div className="form">
                        <form className="register-form">
                        </form>
                        <h3>Product Details</h3>
                        <form className="login-form" onSubmit={this.registerApiCall}>
                            <input type="text" placeholder="Product Name" required onChange={this.onChangeproduct_name} onBlur={this.validateproduct_name}/>
                            <p style={{color:"red"}}>{this.state.product_name_err}</p>
                            <input type="text" placeholder="Product Description" required onChange={this.onChangeproduct_description} onBlur={this.validateproduct_description}/>
                            <p style={{color:"red"}}>{this.state.product_description_err}</p>
                            <input type="text" placeholder="Product Price" required onChange={this.onChangeproduct_price} onBlur={this.validateproduct_price}/>
                            <p style={{color:"red"}}>{this.state.product_price_err}</p>
                            <input type="text" placeholder="Discount" required onChange={this.onChangediscount} onBlur={this.validatediscount}/>
                            <p style={{color:"red"}}>{this.state.discount_err}</p>
                            <input type="text" placeholder="Merchant Name" required onChange={this.onChangemerchant_name} onBlur={this.validatemerchant_name}/>
                            <p style={{color:"red"}}>{this.state.merchant_name_err}</p>
                            <input type="text" placeholder="Quantity" required onChange={this.onChangequantity} onBlur={this.validatequantity}/>
                            <p style={{color:"red"}}>{this.state.quantity_err}</p>
                            <button>Register</button>
                            <Link to='/'><button>Back to Home</button></Link>

                        </form>
                    </div>
                </div>
            </div>
        
        );
    }
}


export default Postdata;

