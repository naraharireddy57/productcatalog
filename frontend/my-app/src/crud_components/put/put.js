import React from "react";
import { Link } from "react-router-dom";
import "./put.css";
import {
    _product_name_checker,
    _new_product_price_checker,
    _return_object_keys,
} from "../validations/helperFunction.js";

class Putdata extends React.Component {
    state = {
        product_name: "",
        product_name_err: "",
        new_product_price: "",
        new_product_price_err: "",
        product_name_db_err: "",


    };

    registerSuccess = () => {
        const { history } = this.props
        history.push("/putStatusCode")
    };

    apiCallFail = (data) => {
        this.setState({ product_name_db_err: data.msg })
        // this.setState({ isSignUp: true, error_msg: data.status_code })
    };

    registerApiCall = async (event) => {
        event.preventDefault();
        console.log(this.state);

        const { product_name, new_product_price } = this.state
        const url = "http://localhost:5000/putdetails"
        const productdetails = {
            product_name,
            new_product_price,


        }
        const option = {
            method: "PUT",
            body: JSON.stringify(productdetails),
            headers: {
                "Content-Type": "application/json",
                Accept: "application/json"
            }
        }
        const response = await fetch(url, option)
        const data = await response.json()
        console.log(data);
        if (data.statuscode === 200) {
            this.registerSuccess()
        }
        else {
            this.apiCallFail(data);
        }
    }

    onChangeproduct_name = (event) => { 
        this.setState({ product_name: event.target.value, product_name_err:"" })
    }
    validateproduct_name = () => {
        const product_name = this.state.product_name;
        const product_name_errors = _product_name_checker(product_name);
        const is_product_name_validated =
        _return_object_keys(product_name_errors).length === 0;
        console.log(product_name_errors);
        console.log(is_product_name_validated);
        if (!is_product_name_validated) {
          // product_name validation failed
          this.setState({ product_name_err: product_name_errors.product_name });
        }
      };

    onChangenew_product_price = (event) => {
        this.setState({ new_product_price: event.target.value, new_product_price_err:"" })
    }
    validatenew_product_price = () => {
        const new_product_price = this.state.new_product_price;
        const new_product_price_errors = _new_product_price_checker(new_product_price);
        const is_new_product_price_validated =
        _return_object_keys(new_product_price_errors).length === 0;
        console.log(new_product_price_errors);
        console.log(is_new_product_price_validated);
        if (!is_new_product_price_validated) {
         
          this.setState({ new_product_price_err: new_product_price_errors.new_product_price });
        }
      };
    

    render() {

        return (
            <div className="response">
                <div className="login-page">
                    <div className="form">
                        <form className="register-form">
                        </form>
                        <h2>Update Product Details</h2>
                        <form className="login-form" onSubmit={this.registerApiCall}>

                            <input type="text" placeholder="Product Name" required onChange={this.onChangeproduct_name} onBlur={this.validateproduct_name} />
                            <p style={{color:"red"}}>{this.state.product_name_err}</p>
                            <p style={{color:"red"}}>{this.state.product_name_db_err}</p>
                            <input type="text" placeholder="New Product price" required onChange={this.onChangenew_product_price} onBlur={this.validatenew_product_price}/>
                            <p style={{color:"red"}}>{this.state.new_product_price_err}</p>
                            <button>Update</button>
                            <Link to='/'><button>Back to Home</button></Link>

                        </form>
                    </div>
                </div>
            </div>


        );
    }
}


export default Putdata;
