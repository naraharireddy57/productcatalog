
import React from "react";
import { Link } from "react-router-dom";

import "./landingpage.css";

class Landingpage extends React.Component {

    render() {
        const myStyle = {
            backgroundImage: "url(https://images.unsplash.com/photo-1523275335684-37898b6baf30?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8Mnx8cHJvZHVjdHxlbnwwfHwwfHw%3D&w=1000&q=80)",
            height: '140vh',
            marginTop: '-70px',
            fontSize: '50px',
            backgroundSize: 'cover',
            backgroundRepeat: 'no-repeat',
        };

        return (
            <div style={myStyle} >
                <div className="response">

                    <div className="login-page">
                        <h2>PRODUCT CATALOG</h2>
                        <div className="form">
                            <h3>Managing Product Details</h3>

                            <Link to='/postdetails'>
                                <button>Create Details</button>
                            </Link>

                            <Link to='/getdetails'>
                                <button>Retrieve Details</button>
                            </Link>

                            <Link to='/getalldetails'>
                                <button>Retrieve All Details</button>
                            </Link>

                            <Link to='/putdetails'>
                                <button>Update Details</button>
                            </Link>

                            <Link to='/deletedetails'>
                                <button>Delete Details</button>
                            </Link>
                        </div>
                    </div>
                </div>
            </div>

        );
    }
}

export default Landingpage;
