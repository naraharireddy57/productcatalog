

import React from "react";

import "./successpage.css";

class StatusCode extends React.Component {
  render() {
    return (
      <div className="ww-background-wrapper">
        <div className="hello">
          <div className="success-icon">
            <svg fill="#000000" height={72} viewBox="0 0 24 24" width={72} xmlns="http://www.w3.org/2000/svg">
              <path d="M0 0h24v24H0z" fill="none" />
              <path d="M9 16.2L4.8 12l-1.4 1.4L9 19 21 7l-1.4-1.4L9 16.2z" />
            </svg>
          </div>
          <h3 className="ww-successtext">
            Data created successfully!!....</h3>
        </div>
      </div>

    );
  }
}

export default StatusCode;
