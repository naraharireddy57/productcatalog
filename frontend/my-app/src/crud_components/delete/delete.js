
import React from "react";
import { Link } from "react-router-dom";
import "./delete.css";
import {
    _product_name_checker,
    _return_object_keys,
} from "../validations/helperFunction.js";

class Deletedata extends React.Component {
        state = {
            product_name: "",
            product_name_err: "",
            product_name_db_err: "",

        };

        registerSuccess = () => {
            const { history } = this.props
            history.push("/deleteStatusCode")
        }

        apiCallFail = (data) => {
            this.setState({ product_name_db_err: data.msg })
            // this.setState({ isSignUp: true, error_msg: data.status_message })
        }

        registerApiCall = async (event) => {
            event.preventDefault();
            console.log(this.state);


            const { product_name } = this.state
            const url = "http://localhost:5000/deletedetails"
            const productdetails = {
                product_name,

            }
            const option = {
                method: "DELETE",
                body: JSON.stringify(productdetails),
                headers: {
                    "Content-Type": "application/json",
                    Accept: "application/json"
                },
            };
            const response = await fetch(url, option)
            const data = await response.json()
            console.log(data);
            if (data.statuscode === 200) {
                this.registerSuccess()
            }
            else {
                this.apiCallFail(data)
            }
        }

        onChangeproduct_name = (event) => {
            this.setState({ product_name: event.target.value, product_name_err:"" })
        }
        validateproduct_name = () => {
            const product_name = this.state.product_name;
            const product_name_errors = _product_name_checker(product_name);
            const is_product_name_validated =
                _return_object_keys(product_name_errors).length === 0;
            console.log(product_name_errors);
            console.log(is_product_name_validated);
            if (!is_product_name_validated) {
                // product_name validation failed
                this.setState({ product_name_err: product_name_errors.product_name });
            }
        };

        render() {

            return (


                <div className="response">

                    <div className="login-page">
                        <div className="form">
                            <form className="register-form">
                            </form>
                            <h2>Delete Product Details</h2>
                            <form className="login-form" onSubmit={this.registerApiCall}>
                                <input type="text" placeholder="Product Name" required onChange={this.onChangeproduct_name} onBlur={this.validateproduct_name} />
                                <p style={{ color: "red" }}>{this.state.product_name_err}</p>
                                <p style={{ color: "red" }}>{this.state.product_name_db_err}</p>
                                <button>Delete</button>

                                <Link to='/'><button>Back to Home</button></Link>

                            </form>
                        </div>
                    </div>
                </div>

            );
        }
    }

export default Deletedata;
