

import React, { useEffect, useState } from "react";
import "./get.css";
import {
    Table,
    TableHead,
    TableCell,
    TableRow,
    TableBody,
    Button,
    makeStyles,
} from "@material-ui/core";
const useStyles = makeStyles({
    table: {
        width: "100%",
        margin: "5px 0 0 5px",
    },
    thead: {
        "& > *": {
            fontSize: 15,
            background: "grey",
            color: "#FFFFFF",
        },
    },
    row: {
        "& > *": {
            fontSize: 13,
        },
    },
});
function GetAlldata() {
    const [productdata, setproductdata] = useState([])
    const classes = useStyles();
    useEffect(async () => {
        // const url = "http://localhost:5000/getalldetails"
        const option = {
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json"
            },
        };
        const response = await fetch("http://localhost:5000/getalldetails", option)
        const data = await response.json()
        console.log(data);
        setproductdata(data)

    }, [])

    return (
        <Table className={classes.table}>
            <TableHead>
                <TableRow className={classes.thead}>
                    <TableCell>Product Name</TableCell>
                    <TableCell>Product Description</TableCell>
                    <TableCell>Product Price</TableCell>
                    <TableCell>Discount</TableCell>
                    <TableCell>Merchant Name</TableCell>
                    <TableCell>Quantity</TableCell>
                </TableRow>
            </TableHead>
            <TableBody>
                {productdata.map((product) => (
                    <TableRow className={classes.row} key={product.id}>
                        <TableCell>{product.product_name}</TableCell>
                        <TableCell>{product.product_description}</TableCell>
                        <TableCell>{product.product_price}</TableCell>
                        <TableCell>{product.discount}</TableCell>
                        <TableCell>{product.merchant_name}</TableCell>
                        <TableCell>{product.quantity}</TableCell>
                    </TableRow>

                ))}
            </TableBody>
            <br></br>

            <Button
                color="primary"
                variant="contained"
                style={{ marginRight: 10 }}
            //   component={Link}
            //   to={`/putdetails`}
            >
                Edit
            </Button>

            <Button
                color="primary"
                variant="contained"
                style={{ marginRight: 10 }}
            //   component={Link}
            //   to={`/deletedetails`}
            >
                Delete
            </Button>
        </Table>

    );
}

export default GetAlldata;
