
import {
  Table,
  TableHead,
  TableCell,
  TableRow,
  TableBody,
  Button,
  makeStyles,
} from "@material-ui/core";
import { Link } from "react-router-dom";

const useStyles = makeStyles({
  table: {
    width: "100%",
    margin: "5px 0 0 5px",
  },
  thead: {
    "& > *": {
      fontSize: 15,
      background: "grey",
      color: "#FFFFFF",
    },
  },
  row: {
    "& > *": {
      fontSize: 13,
    },
  },
});

const ProductList = () => {
  console.log("hello")
  const classes = useStyles();
  const data = localStorage.getItem("productdetails")
  const productdata = JSON.parse(data)
  console.log(productdata)
  return (
    <Table className={classes.table}>
      <TableHead>
        <TableRow className={classes.thead}>
          <TableCell>Product Name</TableCell>
          <TableCell>Product Description</TableCell>
          <TableCell>Product Price</TableCell>
          <TableCell>Discount</TableCell>
          <TableCell>Merchant Name</TableCell>
          <TableCell>Quantity</TableCell>
        </TableRow>
      </TableHead>
      <TableBody>
        <TableRow className={classes.row}>
          <TableCell>{productdata.data.product_name}</TableCell>
          <TableCell>{productdata.data.product_description}</TableCell>
          <TableCell>{productdata.data.product_price}</TableCell>
          <TableCell>{productdata.data.discount}</TableCell>
          <TableCell>{productdata.data.merchant_name}</TableCell>
          <TableCell>{productdata.data.quantity}</TableCell>
        </TableRow>

      </TableBody>
      <br></br>

      <Button
        color="primary"
        variant="contained"
        style={{ marginRight: 10 }}
        component={Link}
        to={`/putdetails`}
      >
        Edit
      </Button>

      <Button
        color="primary"
        variant="contained"
        style={{ marginRight: 10 }}
        component={Link}
        to={`/deletedetails`}
      >
        Delete
      </Button>
    </Table>
  );
};

export default ProductList;
