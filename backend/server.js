//import express..
import express from "express";
//import body-parser..
import bodyParser from "body-parser";
//import post handler...
import { Post_handler } from "./pathhandlers/post/post_details.js";

//import put handler.
import { Put_handler } from "./pathhandlers/put/put_details.js";
//import delete handler..
import { Delete_handler } from "./pathhandlers/delete/delete_details.js";

import cors from "cors";
import Get_handler from "./pathhandlers/get/get_details.js";
import GetAll_handler from "./pathhandlers/get/get_alldetails.js";



const app = express()

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors({ origin: '*' }));

//............post api call....................................
app.post("/postdetails", Post_handler)

//............get api call.....................................
app.post("/getdetails", Get_handler)

app.get("/getalldetails", GetAll_handler)

//............put api call.....................................
app.put("/putdetails", Put_handler)

//............delete api call...................................
app.delete("/deletedetails", Delete_handler)


//............listening port number.......................
app.listen(5000, () => {
    console.log("The server is running 5000")
})
