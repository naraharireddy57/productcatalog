
//import product model...
import { ProductModel } from "../../utils/model/models.js";

function Delete_handler(request, response) {
    const { product_name } = request.body;
    ProductModel.findOne({ product_name }, (err, dataObj) => {
        if (err) {
            response.send(500).send("db err", err)
        } else {
            if (dataObj === null) {
                response.send(400).send({ msg: "unable to find product details" })
            } else {
                dataObj.delete((err) => {
                    if (err) {
                        response.send("Unable to delete data")
                    } else {
                        response.send("Deleted successfully")
                    }
                })
            }

        }
    })
}
export { Delete_handler }