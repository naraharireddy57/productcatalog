
//import product model..
import { ProductModel } from "../../utils/model/models.js";

function Post_handler(request, response) {
    const { product_name, product_description, product_price, discount, merchant_name, quantity } = request.body
    const product_data = { product_name, product_description, product_price, discount, merchant_name, quantity };

    const productdata = new ProductModel(product_data);

    productdata.save().then(() => {
        console.log("data saved")
        response.send({ msg: "data stored successfully" })
    })
        .catch((err) => {
            console.log(err);
        });


}

export { Post_handler }