
//import product model..
import { ProductModel } from "../../utils/model/models.js";

function Put_handler(request, response) {
    const { product_name, new_merchant_name } = request.body;
    ProductModel.findOne({ product_name }, (err, dataObj) => {
        if (err) {
            response.send(500).send("db error", err)
        } else {
            if (dataObj === null) {
                response.send(400).send("data unable to find")
            } else {
                dataObj.updateOne({
                    $set: { merchant_name: new_merchant_name }
                },
                    (err) => {
                        if (err) {
                            response.send("unable to update merchant name")
                        } else {
                            response.send("merchant name updated successfully")
                        }
                    })
            }
        }
    })
}
export { Put_handler }
