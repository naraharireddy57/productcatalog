
//import product models..
import { ProductModel } from "../../utils/model/models.js";

function GetAll_handler(req, res) {
    ProductModel.find({  }, "product_name product_description product_price discount merchant_name quantity -_id", (err, data) => {
        //if given data is error shows 500 database error..
        if (err)  {
            res.status(500).send("database err", err);
        }  else {
                //data object is not null shows 200....
                res.status(200).json(data);
            }
        }
    )
}
export default GetAll_handler;      
