
//import product models..
import { ProductModel } from "../../utils/model/models.js";

function Get_handler(request, response) {
    const { product_name } = request.body;
    ProductModel.findOne({ product_name }, "product_name product_price discount merchant_name quantity -_id", (err, dataObj) => {
        //if given data is error shows 500 database error..
        if (err) {
            response.status(500).send("database err", err)
        } else {
            //data object is null shows data not found...
            if (dataObj === null) {
                response.send("data not found with that product_name")
            } else {
                //data object is not null shows 200....
                response.status(200).send(dataObj)
            }
        }
    })
}
export { Get_handler }